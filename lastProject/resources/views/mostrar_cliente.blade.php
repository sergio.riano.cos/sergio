@extends('layouts.app')
@section('content')
@inject('paises', 'App\Services\Paises')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            <div class="panel-heading text-center"><h3>Datos del cliente {{$cliente->nombre}} {{$cliente->apellido}}</h3></div>

                <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{action('ClienteController@update', ['id_Cliente' => $cliente->id_Cliente])}}">
                        {{ csrf_field() }}             
                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="col-md-4 control-label">Nombre: </label>
                            <div class="col-md-6">
                            <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$cliente->nombre or ''}}"/>
                            @if ($errors->has('nombre'))
                             <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                             </span>
                            @endif
                            </div>
                       </div>
                       <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                           <label for="apellido" class="col-md-4 control-label">Apellido: </label>
                           <div class="col-md-6">
                           <input id="apellido" type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{$cliente->apellido or ''}}"/>
                           @if ($errors->has('apellido'))
                           <span class="help-block">
                              <strong>{{ $errors->first('apellido') }}</strong>
                           </span>
                           @endif
                           </div>
                      </div>
                      <div class="form-group">
                        <label for="sexo" class="col-md-4 control-label">Sexo: </label>
                         <div class="col-md-6">
                          <select name="sexo" id="sexo" class="form-control">
                            <option>
                               {{$cliente->sexo}}
                            </option>
                            <option>
                               Masculino
                            </option>
                            <option>
                               Femenino
                            </option>
                            <option>
                               Otro...
                           </option>
                          </select>
                        </div>
                       </div>                    
                       <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                        <label for="telefono" class="col-md-4 control-label">Teléfono: </label>
                        <div class="col-md-6">
                        <input id="telefono" type="text" class="form-control" name="telefono" placeholder="Telefono" value="{{$cliente->telefono or ''}}"/>
                        @if ($errors->has('telefono'))
                         <span class="help-block">
                           <strong>{{ $errors->first('telefono') }}</strong>
                         </span>
                        @endif
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pais" class="col-md-4 control-label">País:</label>
                        <div class="col-md-6">
                        <select id="pais_Id" name="pais_Id" class="form-control">                            
                              @foreach ($paises->get() as $index => $pais)
                                <option value="{{$index}}" {{old('pais_Id') == $index}}>
                                 {{$pais}}
                                </option>     
                              @endforeach
                            </select>    
                        </div>    
                      </div> 
                      <div class="form-group{{ $errors->has('departamento_Id') ? ' has-error' : '' }}">
                        <label for="departamento" class="col-md-4 control-label">Departamento: </label>
                        <div class="col-md-6">
                        <select id="departamento_Id" data-old="{{old('departamento_Id')}}" name="departamento_Id" class="form-control">                                                                                    
                          <option value="0">Seleccione un nuevo departamento...</option>
                          @foreach ($departamentos as $index => $departamento)
                          <option value="{{$departamento->id_departamento}}" {{$departamento->id_departamento == $id_Departamento ? 'selected' : ''}}>
                           {{$departamento->nombreDepartamento}}
                           </option>    
                        @endforeach
                        </select>  
                            @if ($errors->has('departamento_Id'))
                            <span class="help-block">
                              <strong>{{ $errors->first('departamento_Id') }}</strong>
                            </span>
                           @endif  
                        </div>    
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                           <button type="submit" class="btn btn-primary">Actualizar</button>                           
                        </div>                                                                       
                      </div>                                                                                            
                    </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('sweet::alert')
@endsection
@section('scripts')    
   <script src="{{asset('js/app.js')}}" defer></script>
   <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
   <script src="{{asset('js/miScript.js')}}"></script>
@endsection
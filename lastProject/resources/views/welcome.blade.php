<!doctype html>

<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <style>
           html, body {            
                font-family: 'Raleway', sans-serif;             
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
     <!--Scripts-->
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>
    <body>     
        @extends('layouts.app')
        @section('content')  
        <div class="flex-center position-ref full-height">            
            @auth
            <div class="content">
                <div class="title m-b-md">
                    Hace mucho no te veíamos {{ Auth::user()->name }}!                 
                </div>
                <h3>¡Es bueno tenerte por acá otra vez!</h3>              
            </div>
            @else
            <div class="content">
                <div class="title m-b-md">
                    Te damos la bienvenida!              
                </div>
                <h3>¡Espero que no sea la primera vez que te veamos por aquí!</h3>              
            </div>
            @endauth
        </div>
        
        @include('sweet::alert')
        @endsection
    </body>
</html>

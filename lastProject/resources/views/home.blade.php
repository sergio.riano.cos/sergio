@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                @auth
                <div class="panel-heading text-center"><h3>Lista de Clientes</h3></div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif                   
                    <table id="clientes" class="table table-responsive table-striped">
                        <thead>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Sexo</th>
                            <th>Telefono</th>
                            <th>Pais</th>
                            <th>Departamento</th>              
                            <th>Acciones</th>
                        </thead>
                        <tbody>                            
                            @foreach($clientes as $cliente)
                            <tr>
                            <td>{{$cliente->nombre}}</td>
                            <td>{{$cliente->apellido}}</td>
                            <td>{{$cliente->sexo}}</td>
                            <td>{{$cliente->telefono}}</td>
                            <td>{{$cliente->pais->pais}}</td>
                            <td>{{$cliente->departamento->departamento}}</td>                        
                            <td>
                            <a class="btn btn-warning" href="{{action('ClienteController@edit', ['id_Cliente' => $cliente->id_Cliente])}}">Ver</a>     
                            <a class="btn btn-danger" href="{{action('ClienteController@destroy', ['id_Cliente' => $cliente->id_Cliente])}}">Eliminar</a>                    
                            </td>
                            </tr>
                            @endforeach                           
                        </tbody>
                        <tfoot>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Sexo</th>
                            <th>Teléfono</th>
                            <th>País</th>
                            <th>Departamento</th>
                            <th>Acciones</th>
                        </tfoot>
                    </table>                               
                </div>
                <div class="col-md-offset-5">
                    <a class="btn btn-success"href="{{route('agregar')}}">Crear Cliente</a>
                </div>
            
        </div>
    </div>
</div>
@include('sweet::alert')
@endsection
@section('scripts')
<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('js/DataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/DataTables/datatables.min.css')}}"></script>
<script>
  $(document).ready(function(){
    $('#clientes').DataTable();
  });
</script>
    @else 
    <div class="panel-body text-center"><h3>No puedes pasar!</h3></div>
    @endauth
@endsection
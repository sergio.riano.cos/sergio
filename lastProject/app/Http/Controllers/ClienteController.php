<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Departamento;
use App\Http\Requests\ClienteRequest;
use App\Pais;
use phpDocumentor\Reflection\Types\Integer;

class ClienteController extends Controller
{
    public function ajaxRequest($id) {
        //Petición ajax para obtener los departamentos
        $departamentoDao = new Departamento();
        return Response()->json($departamentoDao->findDepartamentoForPais($id), 200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Método listar por medio del Modelo
        $clientes = Cliente::orderBy('id_Cliente', 'asc')->get(); 
        return view('home', ['clientes' => $clientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Redirección simple
        return view('crear_cliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {
        //Registro obteniendo los datos del formulario por medio de la variable request
        $cliente = Cliente::insert(array(
            'nombre' => $request->input('nombre'),
            'apellido' => $request->input('apellido'),
            'sexo' => $request->input('sexo'),
            'telefono' => (int) $request->input('telefono'),
            'pais_Id' => $request->input('pais_Id'),           
            'departamento_Id' => $request->input('departamento_Id'),
            'usuario_Id' => 1
        ));
        alert()->success('Cliente creado con éxito','Listo');
        return redirect()->action('ClienteController@index')->with('status', 'Cliente registrado correctamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obtener el id del cliente para mostrar su datos en nuevo formulario
        $cliente = Cliente::find($id);
        
        $id_Pais = $cliente->pais_Id;
        $departamentos = Departamento::where('pais_Id', $id_Pais)->get();

        //Referencia a las tablas Pais y Departamento para obtener dichos campos
        $nombrePais = Pais::find($id_Pais);
        $id_Departamento = $cliente->departamento_Id;
        $nombreDepartamento =  Departamento::find($id_Departamento);        
        return view('mostrar_cliente', 
                [
                    'cliente' => $cliente
                ])->with('departamentos', $departamentos)
                ->with('id_Pais', $id_Pais)
                ->with('nombrePais', $nombrePais)
                ->with('id_Departamento', $id_Departamento)
                ->with('nombreDepartameno', $nombreDepartamento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $request, $id)
    {
        //Método para actualizar nuevos campos
        $cliente = Cliente::find($id)->update($request->all());
        alert()->success('Datos actualizados con éxito','Listo');
        return redirect('home')->with('status', 'Datos actualizados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Método eliminar por medio del Id
        $cliente = Cliente::destroy($id);
        alert()->success('El cliente ha sido eliminado','Listo');
        return redirect('home')->with('status', 'Cliente borrad@ correctamente');
    }
}

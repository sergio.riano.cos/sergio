<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|alpha|string|max:20', //Campo obligatorio y máximo 20 caracteres
            'apellido' => 'required|alpha|string|max:20', //Campo obligatorio y máximo 20 caracteres
            'sexo' => 'required', // Campo obligatorio
            'telefono' => 'required|regex:/^[0-9]+$/|min:10|max:10', //Campo obligatorio, solo admite números, y tiene que ser obligatorio de diez de longitud
            'pais_Id' => 'required', //Campo requerido
            'departamento_Id' => 'required' //Campo requerido
        ];
    }

    public function messages()
    {
        return[
          'nombre.required' => 'Campo obligatorio',
          'nombre.max' => 'Debe ingresar menos de 20 carácteres',
          'apellido.required' => 'Campo obligatorio',
          'apellido.max' => 'Debe ingresar menos de 20 carácteres',
          'sexo.required' => 'Campo obligatorio',
          'telefono.required' => 'Campo obligatorio',
          'telefono.max' => 'Ingrese un número de 10 dígitos',
          'telefono.min' => 'Ingrese un número de 10 dígitos',
          'telefono.format' => 'Sólo ingrese números',
          'pais_Id.required' => 'Campo obligatorio',
          'departamento_Id.required' => 'Campo obligatorio'
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    
    protected $table = 'cliente'; 
    public $timestamps = false;
    protected $primaryKey = 'id_Cliente';   
    //Campos necesarios para llenar en la BD
    protected $fillable = ['nombre', 'apellido', 'sexo', 'telefono', 'pais_Id', 'departamento_Id', 'usuario_Id'];

    //Relaciones unos a muchos
    public function user(){
        return $this->belongsTo(User::class,'usuario_Id');
    }

    public function pais(){
       return $this->belongsTo(Pais::class,'pais_Id');
    }

    public function departamento(){
       return $this->belongsTo(Departamento::class, 'departamento_Id');
    }
    
}

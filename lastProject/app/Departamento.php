<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Departamento extends Model
{
    protected $table = 'departamento'; 
    public $timestamps = false;
    protected $primaryKey = 'id_departamento'; 
    //Campos necesarios para llenar en la BD
    protected $fillable = ['id_departamento', 'departamento'];

    //Query que hace el llamado a los departamentos que tengan asociados el id respectivo del país
    public function findDepartamentoForPais($id_pais) {
        $datos = DB::SELECT("SELECT * FROM departamento INNER JOIN pais ON pais.id_Pais=departamento.pais_Id WHERE departamento.pais_Id=:id", [
            "id" => $id_pais
        ]);
        return $datos;
    }

    //Relaciones unos a muchos
    public function cliente(){
        return $this->hasMany(Departamento::class);
    }
}

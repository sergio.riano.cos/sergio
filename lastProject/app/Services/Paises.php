<?php

 namespace App\Services;

 use App\Pais;

 class Paises{

    public function get(){
        //Método que permite el llamado de los datos de la tabla país
        $paises = Pais::get();    
        foreach($paises as $pais)
        {
            $paisesArray[$pais->id_Pais] = $pais->pais;
        }

        return $paisesArray;

    }

 }

?>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'pais'; 
    public $timestamps = false;
    protected $primaryKey = 'id_Pais'; 
    //Campos necesarios para llenar en la BD
    protected $fillable = ['id_Pais', 'pais'];

    //Relaciones unos a muchos
    public function departamentos(){
        return $this->hasMany('departamento');
    }
    public function cliente(){
        return $this->hasMany(Cliente::class);
    }
}

<?php

use App\Pais;
use App\Departamento;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/welcome', 'HomeController@index')->name('home');//Inicio
Route::get('/home', 'ClienteController@index');//Tabla de clientes
Route::get('/crear_cliente', 'ClienteController@create')->name('agregar')->middleware('auth');//Crear Cliente
Route::post('/home', 'ClienteController@store')->name('guardar')->middleware('auth');//Método para crear el cual redirecciona a la Tabla
Route::get('mostrar_cliente/{id_Cliente}/edit', 'ClienteController@edit');//Captura el id del cliente y lo envia al formulario editar
Route::post('home/{id_Cliente}', 'ClienteController@update');//Actualiza los datos y lo reenvia a la tabla de clientes
Route::get('home/{id_Cliente}', 'ClienteController@destroy');//Método eliminar el cual hace una auto-redirección a la tabla
Route::get("/ajaxRequest/{id}", "ClienteController@ajaxRequest");//Petición ajax para mostrar los departamentos
Route::get('/crear_cliente', function(){
    $paises = Pais::all();
    return view('crear_cliente')->with('paises', $paises);//Petición al modelo para traer los países 
})->name('agregar')->middleware('auth');
<?php

use Illuminate\Database\Seeder;
use App\Cliente;
use App\Departamento;
use App\Pais;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Inserta 30 registro ejecutando el comando "php artisan db:seed --class=ClienteSeeder"

        $clien = new Cliente();
        $faker = Faker::create();
        for($i = 0; $i<30; $i++ ){
        DB::table('cliente')->insert([
           'nombre'=>$faker->firstName,
           'apellido'=>$faker->lastName,
           'sexo'=> 'Masculino',
           'telefono'=>$faker->randomNumber,
           'pais_Id' => 2,
           'departamento_Id'=> 6,
           'usuario_Id'=> 1,
        ]);    
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_Cliente');
            $table->String('nombre', 20);
            $table->String('apellido', 20);
            $table->String('sexo', 20);
            $table->bigInteger('telefono');    
            $table->integer('pais_Id')->unsigned();
            $table->foreign('pais_Id', 'fk_Departamento_1')->references('id_Pais')->on('pais')->onDelete('cascade')->onUpdate('restrict');              
            $table->integer('departamento_Id')->unsigned();
            $table->foreign('departamento_Id', 'fk_Departamento_2')->references('id_Departamento')->on('departamento')->onDelete('cascade')->onUpdate('restrict');             
            $table->integer('usuario_Id')->unsigned();
            $table->foreign('usuario_Id', 'fk_Usuario_3')->references('id_Usuario')->on('users')->onDelete('cascade')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
